import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations, setLocations] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [starts, setStarts] = useState('');
    const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value)
    }

    const [ends, setEnds] = useState('');
    const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value)
    }

    const [description, setDescription] = useState('');
    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value)
    }

    const [maxPresentations, setMaxPresentations] = useState('');
    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value)
    }

    const [maxAttendees, setMaxAttendees] = useState('');
    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value)
    }

    const [location, setLocation] = useState('');
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.name = name;
        data.starts = starts;
        data.ends = ends;
        data.description = description;
        data.max_presentations = maxPresentations;
        data.max_attendees = maxAttendees;
        data.location = location;

        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };


        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            setName('');
            setStarts('');
            setEnds('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');
        }
    }
    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';
        const locationResponse = await fetch(locationUrl);
        if (locationResponse.ok) {
            const data = await locationResponse.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Conference</h1>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartsChange} value={starts} placeholder="Start Date" required type="date" name="starts" id="starts" className="form-control" />
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndsChange} value={ends} placeholder="End Date" required type="date" name="ends" id="ends" className="form-control" />
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="mb-3">
                            <label onChange={handleDescriptionChange} value={description} htmlFor="Description" className="form-label">Description</label>
                            <textarea className="form-control" name="description" id="description" rows="3"></textarea>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max Presentations" required type="text" name="max_presentations"
                                id="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">Maximum Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Max Attendees" required type="text" name="max_attendees" id="max_attendees"
                                className="form-control" />
                            <label htmlFor="max_attendees">Maximum Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
                                <option value="">Choose a location</option>
                                {locations.map(location => {
                                    return (
                                        <option key={location.id} value={location.id}>
                                            {location.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;


// HERE IS YOUNG'S CODE
// import React, { useEffect, useState } from 'react';

// function ConferenceForm() {
//     const [locations, setLocations] = useState([]);

//     // Set useState hook
//     const [name, setName] = useState('');
//     const [startDate, setStartDate] = useState('');
//     const [endDate, setEndDate] = useState('');
//     const [description, setDescription] = useState('');
//     const [maxPresentations, setMaxPresentations] = useState('');
//     const [maxAttendees, setMaxAttendees] = useState('');
//     const [location, setLocation] = useState('');

//     // Create handle...Change
//     const handleNameChange = (event) => {
//         const value = event.target.value;
//         setName(value);
//     }
//     const handleStartDateChange = (event) => {
//         const value = event.target.value;
//         setStartDate(value);
//     }
//     const handleEndDateChange = (event) => {
//         const value = event.target.value;
//         setEndDate(value);
//     }
//     const handleDescriptionChange = (event) => {
//         const value = event.target.value;
//         setDescription(value);
//     }
//     const handleMaxPresentationsChange = (event) => {
//         const value = event.target.value;
//         setMaxPresentations(value);
//     }
//     const handleMaxAttendeesChange = (event) => {
//         const value = event.target.value;
//         setMaxAttendees(value);
//     }
//     const handleLocationChange = (event) => {
//         const value = event.target.value;
//         setLocation(value);
//     }


//     // Submit
//     const handleSubmit = async (event) => {
//         event.preventDefault();

//         const data = {};

//         data.name = name;
//         data.starts = startDate;
//         data.ends = endDate;
//         data.description = description;
//         data.max_presentations = maxPresentations;
//         data.max_attendees = maxAttendees;
//         data.location = location;
//         console.log(data);

//         const conferenceUrl = 'http://localhost:8000/api/conferences/';
//         const fetchConfig = {
//             method: "post",
//             body: JSON.stringify(data),
//             headers: {
//                 'Content-Type': 'application/json',
//             },
//         };

//         const response = await fetch(conferenceUrl, fetchConfig);
//         if (response.ok) {
//             const newConference = await response.json();
//             console.log(newConference);

//             setName('');
//             setStartDate('');
//             setEndDate('');
//             setDescription('');
//             setMaxPresentations('');
//             setMaxAttendees('');
//             setLocation('');
//         }
//     }

//     const fetchData = async () => {
//         const url = 'http://localhost:8000/api/locations/';

//         const response = await fetch(url);

//         if (response.ok) {
//             const data = await response.json();
//             setLocations(data.locations);
//         }
//     }

//     useEffect(() => {
//         fetchData();
//     }, []);

//     return (
//         <div className="row">
//         <div className="offset-3 col-6">
//             <div className="shadow p-4 mt-4">
//                 <h1>Create a new conference</h1>
//                 <form onSubmit={handleSubmit} id="create-conference-form">
//                 <div className="form-floating mb-3">
//                     <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
//                     <label htmlFor="name">Name</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleStartDateChange} value={startDate} placeholder="Start Date" required type="datetime-local" name="starts" id="starts" className="form-control" />
//                     <label htmlFor="start-date">Start Date</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleEndDateChange} value={endDate} placeholder="End Date" required type="datetime-local" name="ends" id="ends" className="form-control" />
//                     <label htmlFor="end-date">End Date</label>
//                 </div>
//                 <div className="textarea mb-3">
//                     <textarea onChange={handleDescriptionChange} value={description} name="description" id="description" className="form-control"></textarea>
//                     <label htmlFor="description">Description</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleMaxPresentationsChange} value={maxPresentations} placeholder="Max Presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
//                     <label htmlFor="max_presentations">Max Presentation</label>
//                 </div>
//                 <div className="form-floating mb-3">
//                     <input onChange={handleMaxAttendeesChange} value={maxAttendees} placeholder="Max Attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
//                     <label htmlFor="max_attendees">Max Attendees</label>
//                 </div>
//                 <div className="mb-3">
//                     <select onChange={handleLocationChange} value={location} required id="location" name="location" className="form-select">
//                         <option value="">Choose a Location</option>
//                         {locations.map(location => {
//                             return(
//                                 <option key={location.id} value={location.id}>
//                                     {location.name}
//                                 </option>
//                             );
//                         })}
//                     </select>
//                 </div>
//                 <button className="btn btn-primary">Create</button>
//                 </form>
//             </div>
//         </div>
//         </div>
//     );
// }

// export default ConferenceForm;
