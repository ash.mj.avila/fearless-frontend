import { NavLink } from "react-router-dom";

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container-fluid">
        <a className="navbar-brand" href="#">Conference GO!</a>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li>
              <NavLink className="nav-link" aria-current="page" to="">Home</NavLink>
            </li>
            {/* <li>
            <NavLink className="nav-link" aria-current="page" to="/attendees/new">New Attendees</NavLink>
            </li> */}
            <li>
              <NavLink className="nav-link " aria-current="page" to="/locations/new">New Location</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" aria-current="page" to="/conferences/new">New Conference</NavLink>
            </li>
            <li>
              <NavLink className="nav-link" aria-current="page" to="/presentations/new">New Presentations</NavLink>
            </li>
            {/* <li>
              <a className="nav-link" aria-current="page" href="http://localhost:3000/login.html">Login</a>
            </li> */}
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default Nav;
