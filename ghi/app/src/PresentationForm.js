import React, { useEffect, useState } from 'react';

const PresentationForm = () => {
    const [presentations, setPresentations] = useState([]);

    const [name, setName] = useState('');
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value)
    }

    const [email, setEmail] = useState('');
    const handleEmailChange = (event) => {
        const value = event.target.value;
        setEmail(value)
    }

    const [companyName, setCompanyName] = useState('');
    const handleCompanyNameChange = (event) => {
        const value = event.target.value;
        setCompanyName(value)
    }

    const [title, setTitle] = useState('');
    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value)
    }

    const [synopsis, setSynopsis] = useState('');
    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value)
    }

    const [conference, setConference] = useState('');
    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value)
    }

    // drop down (GET):
    const fetchData = async () => {
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const conferenceResponse = await fetch(conferenceUrl);
        if (conferenceResponse.ok) {
            const data = await conferenceResponse.json();
            setPresentations(data.conferences)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    // handle data upon submit (POST)
    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};

        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        console.log(data);

        const conferenceID = data.conference
        const presentationUrl = `http://localhost:8000${conferenceID}presentations/`;
        console.log(presentationUrl)
        const fetchConfig = {
            // this tells the fetch what to expect
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setName('');
            setEmail('');
            setCompanyName('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Presentation</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Presenter Name" required type="text" name="presenter_name"
                                id="presenter_name" className="form-control" />
                            <label htmlFor="presenter_name">Presenter Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEmailChange} value={email} placeholder="Presenter Email" required type="email" name="presenter_email"
                                id="presenter_email" className="form-control" />
                            <label htmlFor="presenter_email">Presenter Email</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleCompanyNameChange} value={companyName}placeholder="Company Name" type="text" name="company_name" id="company_name"
                                className="form-control" />
                            <label htmlFor="company_name">Company Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" name="title" id="title"
                                className="form-control" />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="mb-3">
                            <label onChange={handleSynopsisChange} value={synopsis} htmlFor="Synopsis" className="form-label">Synopsis</label>
                            <textarea className="form-control" name="synopsis" id="synopsis" rows="3"></textarea>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleConferenceChange} value={conference} required id="conference" name="conference" className="form-select">
                                <option value="">Choose a conference</option>
                                {presentations.map(conference => {
                                    return(
                                        <option key={conference.href} value={conference.href}>
                                            {conference.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default PresentationForm;
