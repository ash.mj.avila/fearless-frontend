function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
        <div class="card shadow p-3 mb-5 bg-body-tertiary rounded">
        <div class=>
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
            <time> ${starts} - ${ends} </time>
            </div>
        </div>
        </div>
    `;
  }

function alert(message) {
        console.log("alert")
            return`<div class="alert alert-primary" role="alert">${message}</div>`;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    const columns = document.querySelectorAll('.col');
    let colIndx = 0;

    try {
      const response = await fetch(url);

        if (!response.ok) {
        // Figure out what to do when the response is bad
            const alerts = document.querySelector(".container");
            // why
            alerts.innerHTML += alert("Nah... it's BROKEN");

        } else {
            const data = await response.json();


            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const name = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = new Date(details.conference.starts).toLocaleDateString();
                    const ends = new Date(details.conference.ends).toLocaleDateString();
                    const location = details.conference.location.name;
                    const html = createCard(name, description, pictureUrl, starts, ends, location);
                    const column = columns[colIndx % 3];
                    column.innerHTML += html;
                    colIndx = (colIndx + 1) % 3
                }
            }
        }
        } catch (e) {
            console.error("This error be... ", e)
        // Figure out what to do if an error is raised
        }

  });
