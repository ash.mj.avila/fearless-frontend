window.addEventListener('DOMContentLoaded', async () => {
  // dropdown code:
    const conferenceUrl = 'http://localhost:8000/api/locations/';
    const conferenceResponse = await fetch(conferenceUrl);
    if (conferenceResponse.ok) {
        const data = await conferenceResponse.json();
        const selectTag = document.getElementById('location');
        for (let location of data.locations) {
            const option = document.createElement('option');
            option.value = location.id;
            option.innerHTML = location.name;
            selectTag.appendChild(option);
        }
    }
  // to generate a form and how to interact with it
  const formTag = document.getElementById('create-conference-form');
  formTag.addEventListener('submit', async (event) => {
    event.preventDefault();

    const formData = new FormData(formTag);
    const json = JSON.stringify(Object.fromEntries(formData));

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig = {
      // this tells the fetch what to expect
      method: "post",
      body: json,
      headers: {
        'Content-Type': 'application/json',
    },
    };

    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      formTag.reset();
      const newConference = await response.json();
      console.log(newConference);
    }
  });
});
