window.addEventListener('DOMContentLoaded', async () => {
    // dropdown code:
    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const conferenceResponse = await fetch(conferenceUrl);
    if (conferenceResponse.ok) {
        const data = await conferenceResponse.json();
        const selectTag = document.getElementById('conference');
        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
    }
    // to generate a form and how to interact with it
    const formTag = document.getElementById('create-presentation-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const confHref = JSON.parse(json)['conference']
        const presentationUrl = `http://localhost:8000${confHref}presentations/`;
        const fetchConfig = {
            // this tells the fetch what to expect
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
            console.log(newPresentation);
        }
    });
});
